import React from "react";
import { MovieItem } from "./MovieItem";

const MovieList = (props) => {
  const renderMovieList = () => {
    return props.movieArr.slice(0, 15).map((movie, index) => {
      return <MovieItem key={index} movie={movie} />;
    });
  };
  return <div className='grid grid-cols-5 gap-5'>{renderMovieList()}</div>;
};

export default MovieList;
