import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export const MovieItem = (props) => {
  const { hinhAnh, tenPhim, moTa, maPhim } = props.movie;
  return (
    <Card
      hoverable
      style={{
        width: "100%",
      }}
      cover={<img className='h-80 object-cover' alt='example' src={hinhAnh} />}
    >
      <Meta
        title={tenPhim}
        description={moTa.length < 80 ? moTa : moTa.slice(0, 80) + "..."}
      />
      <NavLink
        to={`/detail/${maPhim}`}
        className='bg-red-500 px-5 py-2 rounder text-white shadow hover:shadow-xl transition hover:text-white'
      >
        Xem chi tiết
      </NavLink>
    </Card>
  );
};
