import React, { useEffect, useState } from "react";
import { movieService } from "../../../services/movieService";
import { Tabs } from "antd";
import MovieTabItem from "./MovieTabItem";

const onChange = (key) => {
  console.log(key);
};

const MovieTabs = () => {
  const [dataMovie, setDataMovie] = useState([]);

  useEffect(() => {
    movieService
      .getRap()
      .then((res) => {
        setDataMovie(res.data.content);
        console.log(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderRap = () => {
    return dataMovie.map((rap) => {
      return {
        label: <img src={rap.logo} className='w-16 h-16' alt='true' />,
        key: rap.maHeThongRap,
        children: (
          <Tabs
            tabPosition='left'
            defaultActiveKey='1'
            onChange={onChange}
            items={rap.lstCumRap.map((cumRap) => {
              return {
                label: (
                  <div>
                    <p>{cumRap.tenCumRap}</p>
                  </div>
                ),
                key: cumRap.maCumRap,
                children: cumRap.danhSachPhim.map((phim) => {
                  return <MovieTabItem movie={phim} />;
                }),
              };
            })}
          />
        ),
      };
    });
  };

  return (
    <div>
      <Tabs
        tabPosition='left'
        defaultActiveKey='1'
        onChange={onChange}
        items={renderRap()}
      />
    </div>
  );
};

export default MovieTabs;
