import React, { useEffect, useState } from "react";
import { userLocalStorage } from "../../services/localStorageService";
import { movieService } from "../../services/movieService";
import MovieList from "./MovieList/MovieList";
import MovieTabs from "./MovieTabs/MovieTabs";

const HomePage = () => {
  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    movieService
      .getDanhSachPhim()
      .then((res) => {
        console.log(res.data.content);
        setMovieArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <div className='container mx-auto'>
        <MovieList movieArr={movieArr} />
        <MovieTabs />
      </div>
    </div>
  );
};

export default HomePage;
