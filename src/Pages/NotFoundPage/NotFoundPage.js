import React from "react";

const NotFoundPage = () => {
  return (
    <div className='flex justify-center items-center h-screen w-screen'>
      <h4 className='text-4xl text-red-500 font-medium animate-bounce'>404</h4>
    </div>
  );
};

export default NotFoundPage;
