import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userService } from "../../services/userService";
import { Navigate, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SET_USER_INFO } from "../../Components/redux/const/userConst";
import { userLocalStorage } from "../../services/localStorageService";
import Lottie from "lottie-react";
import animate from "../../assets/127954-fifa-world-cup-animation.json";
import {
  setLoginAction,
  setLoginActionService,
} from "../../Components/redux/actions/userAction";

const LoginPage = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onFinish = (values) => {
    userService
      .postDangNhap(values)
      .then((res) => {
        dispatch(setLoginAction(res.data.content));

        message.success("LOGGED IN");

        userLocalStorage.set(res.data.content);
      })
      .catch((err) => {
        message.error("FAILED");
      });
  };

  const onFinishReduxThunk = (value) => {
    let onSuccess = () => {
      setTimeout(() => {
        //   window.location.href = "/";
        navigate("/");
      }, 1000);
    };
    dispatch(setLoginActionService(value, onSuccess));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className='flex justify-center items-center h-screen w-screen'>
      <div className='container p-5 flex'>
        <div className='h-full w-1/2'>
          <Lottie animationData={animate} loop={true} />
        </div>
        <div className='h-full w-1/2 pt-20'>
          <Form
            name='basic'
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishReduxThunk}
            onFinishFailed={onFinishFailed}
            autoComplete='off'
            layout='vertical'
          >
            <Form.Item
              label='Username'
              name='taiKhoan'
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label='Password'
              name='matKhau'
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                span: 24,
              }}
            >
              <Button
                className='bg-red-500 text-white hover:bg-white hover:text-red-600'
                htmlType='submit'
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
