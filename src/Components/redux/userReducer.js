import { userLocalStorage } from "../../services/localStorageService";
import { SET_USER_INFO } from "./const/userConst";

const initialState = {
  userInfo: userLocalStorage.get(),
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_INFO: {
      return { ...state, userInfo: payload };
    }

    default:
      return state;
  }
};
