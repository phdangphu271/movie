import { SET_USER_INFO } from "../const/userConst.js";
import { userService } from "../../../services/userService.js";
import { userLocalStorage } from "../../../services/localStorageService.js";
import { message } from "antd";

export const setLoginAction = (value) => {
  return { type: SET_USER_INFO, payload: value };
};

export const setLoginActionService = (userForm, onSuccess) => {
  return (dispatch) => {
    message.success("LOGGED IN");

    onSuccess();

    userService.postDangNhap(userForm).then((res) => {
      userLocalStorage.set(res.data.content);

      dispatch({ type: SET_USER_INFO, payload: res.data.content });
    });
  };
};
