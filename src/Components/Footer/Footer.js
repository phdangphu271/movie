import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";
import FooterDesktop from "./FooterDesktop";
import FooterMobile from "./FooterMobile";
import FooterTablet from "./FooterTablet";

const Footer = () => {
  return (
    <div>
      <Desktop>
        <FooterDesktop />
      </Desktop>

      <Tablet>
        <FooterTablet />
      </Tablet>

      <Mobile>
        <FooterMobile />
      </Mobile>
    </div>
  );
};

export default Footer;
