import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { userLocalStorage } from "../../services/localStorageService";

const UserNav = () => {
  let user = useSelector((state) => {
    return state.userReducer.userInfo;
  });

  const handleLogout = () => {
    userLocalStorage.remove();

    // reload lai trang de state reset tren redux
    window.location.href = "/login";

    // usenavigate khong load lai trang
  };

  const renderContent = () => {
    if (user) {
      return (
        <>
          <span>{user.hoTen}</span>
          <button
            onClick={handleLogout}
            className='border-2 rounded border-black px-5 py-2'
          >
            Logout
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to='/login'>
            <button className='border-2 rounded border-black px-5 py-2'>
              Login
            </button>
          </NavLink>
          <button className='border-2 rounded border-black px-5 py-2'>
            Sign in
          </button>
        </>
      );
    }
  };
  return <div className='space-x-5'>{renderContent()}</div>;
};

export default UserNav;
