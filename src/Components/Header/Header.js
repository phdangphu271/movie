import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

const Header = () => {
  return (
    <div className='flex px-10 py-5 shadow shadow-blue-300 justify-between items-center'>
      <NavLink to='/'>
        <span className='text-red-600 font-medium text-xl'>CyberFlix</span>
      </NavLink>

      <UserNav />
    </div>
  );
};

export default Header;
