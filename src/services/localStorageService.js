const USER_LOGIN = "USER_LOGIN";

export const userLocalStorage = {
  set: (userData) => {
    const userJson = JSON.stringify(userData);
    localStorage.setItem(USER_LOGIN, userJson);
  },
  get: () => {
    const userJson = localStorage.getItem(USER_LOGIN);

    if (userJson) {
      return JSON.parse(userJson);
    } else {
      return null;
    }
  },
  remove: () => {
    localStorage.removeItem(USER_LOGIN);
  },
};
