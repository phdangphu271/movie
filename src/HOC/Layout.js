import React, { Children } from "react";
import Header from "../Components/Header/Header";
import Footer from "../Components/Footer/Footer";

const Layout = ({ children }) => {
  return (
    <div className='space-y-5'>
      <Header />
      {/* <Component /> */}
      {children}
      <Footer />
    </div>
  );
};

export default Layout;
